#! /bin/bash


echo "***Date***" > /tmp/serverinfo.info
date >> /tmp/serverinfo.info
echo "" >> /tmp/serverinfo.info

echo "***Last 10 Logged In***" >> /tmp/serverinfo.info
last | grep pts | head -10 >> /tmp/serverinfo.info
echo "" >> /tmp/serverinfo.info

echo "***Swap Space***" >> /tmp/serverinfo.info
free -m | grep Swap >> /tmp/serverinfo.info
echo "" >> /tmp/serverinfo.info

echo "***Kernel Version***" >> /tmp/serverinfo.info
uname -or >> /tmp/serverinfo.info
echo "" >> /tmp/serverinfo.info

echo "***IP Address***" >> /tmp/serverinfo.info
nmcli con show static | grep IP4.A >> /tmp/serverinfo.info


